/*
 * G3 API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package co.peaas.map.g3apiswagger.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * PaymentAddress
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-11-09T12:19:59.555+05:30")
public class PaymentAddress {
  @SerializedName("partialUpdate")
  private Boolean partialUpdate = null;

  @SerializedName("addressID")
  private Integer addressID = null;

  @SerializedName("customerID")
  private Integer customerID = null;

  @SerializedName("accountID")
  private Integer accountID = null;

  @SerializedName("defaultAddress")
  private Boolean defaultAddress = null;

  @SerializedName("csrUpdate")
  private Integer csrUpdate = null;

  @SerializedName("errorMessage")
  private String errorMessage = null;

  @SerializedName("selectedAddress")
  private String selectedAddress = null;

  @SerializedName("addresses")
  private List<Address> addresses = new ArrayList<Address>() ;

  @SerializedName("address1")
  private String address1 = null;

  @SerializedName("address2")
  private String address2 = null;

  @SerializedName("residenceNo")
  private Object residenceNo = null;

  @SerializedName("aptNo")
  private Object aptNo = null;

  @SerializedName("streetName")
  private Object streetName = null;

  @SerializedName("city")
  private Object city = null;

  @SerializedName("province")
  private Object province = null;

  @SerializedName("postalCode")
  private Object postalCode = null;

  @SerializedName("country")
  private Object country = null;

  public PaymentAddress partialUpdate(Boolean partialUpdate) {
    this.partialUpdate = partialUpdate;
    return this;
  }

   /**
   * Get partialUpdate
   * @return partialUpdate
  **/
  @ApiModelProperty(example = "null", value = "")
  public Boolean getPartialUpdate() {
    return partialUpdate;
  }

  public void setPartialUpdate(Boolean partialUpdate) {
    this.partialUpdate = partialUpdate;
  }

  public PaymentAddress addressID(Integer addressID) {
    this.addressID = addressID;
    return this;
  }

   /**
   * Get addressID
   * @return addressID
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getAddressID() {
    return addressID;
  }

  public void setAddressID(Integer addressID) {
    this.addressID = addressID;
  }

  public PaymentAddress customerID(Integer customerID) {
    this.customerID = customerID;
    return this;
  }

   /**
   * Get customerID
   * @return customerID
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getCustomerID() {
    return customerID;
  }

  public void setCustomerID(Integer customerID) {
    this.customerID = customerID;
  }

  public PaymentAddress accountID(Integer accountID) {
    this.accountID = accountID;
    return this;
  }

   /**
   * Get accountID
   * @return accountID
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getAccountID() {
    return accountID;
  }

  public void setAccountID(Integer accountID) {
    this.accountID = accountID;
  }

  public PaymentAddress defaultAddress(Boolean defaultAddress) {
    this.defaultAddress = defaultAddress;
    return this;
  }

   /**
   * Get defaultAddress
   * @return defaultAddress
  **/
  @ApiModelProperty(example = "null", value = "")
  public Boolean getDefaultAddress() {
    return defaultAddress;
  }

  public void setDefaultAddress(Boolean defaultAddress) {
    this.defaultAddress = defaultAddress;
  }

  public PaymentAddress csrUpdate(Integer csrUpdate) {
    this.csrUpdate = csrUpdate;
    return this;
  }

   /**
   * Get csrUpdate
   * @return csrUpdate
  **/
  @ApiModelProperty(example = "null", value = "")
  public Integer getCsrUpdate() {
    return csrUpdate;
  }

  public void setCsrUpdate(Integer csrUpdate) {
    this.csrUpdate = csrUpdate;
  }

  public PaymentAddress errorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
    return this;
  }

   /**
   * Get errorMessage
   * @return errorMessage
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getErrorMessage() {
    return errorMessage;
  }

  public void setErrorMessage(String errorMessage) {
    this.errorMessage = errorMessage;
  }

  public PaymentAddress selectedAddress(String selectedAddress) {
    this.selectedAddress = selectedAddress;
    return this;
  }

   /**
   * Get selectedAddress
   * @return selectedAddress
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getSelectedAddress() {
    return selectedAddress;
  }

  public void setSelectedAddress(String selectedAddress) {
    this.selectedAddress = selectedAddress;
  }

  public PaymentAddress addresses(List<Address> addresses) {
    this.addresses = addresses;
    return this;
  }

  public PaymentAddress addAddressesItem(Address addressesItem) {
    if (this.addresses == null) {
      this.addresses = new ArrayList<Address>();
    }
    this.addresses.add(addressesItem);
    return this;
  }

   /**
   * Get addresses
   * @return addresses
  **/
  @ApiModelProperty(example = "null", value = "")
  public List<Address> getAddresses() {
    return addresses;
  }

  public void setAddresses(List<Address> addresses) {
    this.addresses = addresses;
  }

  public PaymentAddress address1(String address1) {
    this.address1 = address1;
    return this;
  }

   /**
   * Get address1
   * @return address1
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getAddress1() {
    return address1;
  }

  public void setAddress1(String address1) {
    this.address1 = address1;
  }

  public PaymentAddress address2(String address2) {
    this.address2 = address2;
    return this;
  }

   /**
   * Get address2
   * @return address2
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getAddress2() {
    return address2;
  }

  public void setAddress2(String address2) {
    this.address2 = address2;
  }

  public PaymentAddress residenceNo(Object residenceNo) {
    this.residenceNo = residenceNo;
    return this;
  }

   /**
   * Get residenceNo
   * @return residenceNo
  **/
  @ApiModelProperty(example = "null", value = "")
  public Object getResidenceNo() {
    return residenceNo;
  }

  public void setResidenceNo(Object residenceNo) {
    this.residenceNo = residenceNo;
  }

  public PaymentAddress aptNo(Object aptNo) {
    this.aptNo = aptNo;
    return this;
  }

   /**
   * Get aptNo
   * @return aptNo
  **/
  @ApiModelProperty(example = "null", value = "")
  public Object getAptNo() {
    return aptNo;
  }

  public void setAptNo(Object aptNo) {
    this.aptNo = aptNo;
  }

  public PaymentAddress streetName(Object streetName) {
    this.streetName = streetName;
    return this;
  }

   /**
   * Get streetName
   * @return streetName
  **/
  @ApiModelProperty(example = "null", value = "")
  public Object getStreetName() {
    return streetName;
  }

  public void setStreetName(Object streetName) {
    this.streetName = streetName;
  }

  public PaymentAddress city(Object city) {
    this.city = city;
    return this;
  }

   /**
   * Get city
   * @return city
  **/
  @ApiModelProperty(example = "null", value = "")
  public Object getCity() {
    return city;
  }

  public void setCity(Object city) {
    this.city = city;
  }

  public PaymentAddress province(Object province) {
    this.province = province;
    return this;
  }

   /**
   * Get province
   * @return province
  **/
  @ApiModelProperty(example = "null", value = "")
  public Object getProvince() {
    return province;
  }

  public void setProvince(Object province) {
    this.province = province;
  }

  public PaymentAddress postalCode(Object postalCode) {
    this.postalCode = postalCode;
    return this;
  }

   /**
   * Get postalCode
   * @return postalCode
  **/
  @ApiModelProperty(example = "null", value = "")
  public Object getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(Object postalCode) {
    this.postalCode = postalCode;
  }

  public PaymentAddress country(Object country) {
    this.country = country;
    return this;
  }

   /**
   * Get country
   * @return country
  **/
  @ApiModelProperty(example = "null", value = "")
  public Object getCountry() {
    return country;
  }

  public void setCountry(Object country) {
    this.country = country;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PaymentAddress paymentAddress = (PaymentAddress) o;
    return Objects.equals(this.partialUpdate, paymentAddress.partialUpdate) &&
        Objects.equals(this.addressID, paymentAddress.addressID) &&
        Objects.equals(this.customerID, paymentAddress.customerID) &&
        Objects.equals(this.accountID, paymentAddress.accountID) &&
        Objects.equals(this.defaultAddress, paymentAddress.defaultAddress) &&
        Objects.equals(this.csrUpdate, paymentAddress.csrUpdate) &&
        Objects.equals(this.errorMessage, paymentAddress.errorMessage) &&
        Objects.equals(this.selectedAddress, paymentAddress.selectedAddress) &&
        Objects.equals(this.addresses, paymentAddress.addresses) &&
        Objects.equals(this.address1, paymentAddress.address1) &&
        Objects.equals(this.address2, paymentAddress.address2) &&
        Objects.equals(this.residenceNo, paymentAddress.residenceNo) &&
        Objects.equals(this.aptNo, paymentAddress.aptNo) &&
        Objects.equals(this.streetName, paymentAddress.streetName) &&
        Objects.equals(this.city, paymentAddress.city) &&
        Objects.equals(this.province, paymentAddress.province) &&
        Objects.equals(this.postalCode, paymentAddress.postalCode) &&
        Objects.equals(this.country, paymentAddress.country);
  }

  @Override
  public int hashCode() {
    return Objects.hash(partialUpdate, addressID, customerID, accountID, defaultAddress, csrUpdate, errorMessage, selectedAddress, addresses, address1, address2, residenceNo, aptNo, streetName, city, province, postalCode, country);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PaymentAddress {\n");
    
    sb.append("    partialUpdate: ").append(toIndentedString(partialUpdate)).append("\n");
    sb.append("    addressID: ").append(toIndentedString(addressID)).append("\n");
    sb.append("    customerID: ").append(toIndentedString(customerID)).append("\n");
    sb.append("    accountID: ").append(toIndentedString(accountID)).append("\n");
    sb.append("    defaultAddress: ").append(toIndentedString(defaultAddress)).append("\n");
    sb.append("    csrUpdate: ").append(toIndentedString(csrUpdate)).append("\n");
    sb.append("    errorMessage: ").append(toIndentedString(errorMessage)).append("\n");
    sb.append("    selectedAddress: ").append(toIndentedString(selectedAddress)).append("\n");
    sb.append("    addresses: ").append(toIndentedString(addresses)).append("\n");
    sb.append("    address1: ").append(toIndentedString(address1)).append("\n");
    sb.append("    address2: ").append(toIndentedString(address2)).append("\n");
    sb.append("    residenceNo: ").append(toIndentedString(residenceNo)).append("\n");
    sb.append("    aptNo: ").append(toIndentedString(aptNo)).append("\n");
    sb.append("    streetName: ").append(toIndentedString(streetName)).append("\n");
    sb.append("    city: ").append(toIndentedString(city)).append("\n");
    sb.append("    province: ").append(toIndentedString(province)).append("\n");
    sb.append("    postalCode: ").append(toIndentedString(postalCode)).append("\n");
    sb.append("    country: ").append(toIndentedString(country)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

