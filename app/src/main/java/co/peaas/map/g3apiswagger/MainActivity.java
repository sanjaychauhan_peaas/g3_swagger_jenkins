package co.peaas.map.g3apiswagger;

import android.os.AsyncTask;
import android.os.Debug;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.testfairy.TestFairy;

import org.junit.runner.JUnitCore;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import co.peaas.map.g3apiswagger.api.DefaultApi;
import co.peaas.map.g3apiswagger.model.InitProvisionRequest;
import co.peaas.map.g3apiswagger.model.InitProvisionResponse;
import co.peaas.map.g3apiswagger.model.ResendOTPRequest;
import co.peaas.map.g3apiswagger.model.ResendOTPResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity  {

    private DefaultApi defaultApi;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_call_api = (Button) findViewById(R.id.btn_call_api);

        defaultApi = new ApiClient().createService(DefaultApi.class);


        TestFairy.begin(this, "abf50cbafa777fee3231f51f06108f6b629c475f");



        btn_call_api.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                JUnitCore junit = new JUnitCore();
                junit.run(TestAllApiActivity.class);






      /*          InitProvisionRequest init = new InitProvisionRequest();
               init.setApplicationVersion("1.0.2");
                init.setContactPhone("4168791223");
                init.setCountryCode("CA");
               init.setDeviceID("959595959595959");
                init.setEmail("mobiledev@peaas.co");
                init.setPlatform("Android");

                Call<InitProvisionResponse> requestCall = defaultApi.initProvisioningPost(init);
                try {
                    Response<InitProvisionResponse> response = requestCall.execute();
                    Log.d("MainActivity","onResponse"+response.toString());
                    Log.d("MainActivity","onResponse"+response.body());
                    Log.d("MainActivity","onResponse"+response.body().getEmail());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                requestCall.enqueue(new Callback<InitProvisionResponse>() {
                   @Override
                   public void onResponse(Call<InitProvisionResponse> call, Response<InitProvisionResponse> response) {
                       Log.d("MainActivity","onResponse"+response.toString());
                       Log.d("MainActivity","onResponse"+response.body());
                       Log.d("MainActivity","onResponse"+response.body().getEmail());
                   }

                    @Override
                   public void onFailure(Call<InitProvisionResponse> call, Throwable t) {
                        t.printStackTrace();
                        Log.d("MainActivity","onFailure");
                    }
               });
*/

//                ResendOTPRequest request = new ResendOTPRequest();
//                request.setCustomerId("471293");
//                request.setContactPhone("4168791223");
//                // ResendOTPResponse response = api.resendOtpPost(init);
//
//                Call<ResendOTPResponse> response = defaultApi.resendOtpPost(request);
//
//
//                response.enqueue(new Callback<ResendOTPResponse>() {
//                    @Override
//                    public void onResponse(Call<ResendOTPResponse> call, Response<ResendOTPResponse> response) {
//                        Log.d("MainActivity","onResponse"+response.toString());
//                        Log.d("MainActivity","onResponse"+response.body());
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<ResendOTPResponse> call, Throwable t) {
//                        t.printStackTrace();
//                        Log.d("MainActivity","onFailure");
//                    }
//                });
//
//
//            }
//        });


//
//
//                GetProfileInfoRequest request = new GetProfileInfoRequest();
//                request.setCustomerId("471293");
//
//
//                Call<GetProfileInfoResponse> response = defaultApi.getProfileInfoPost(request);
//
//
//                response.enqueue(new Callback<GetProfileInfoResponse>() {
//                    @Override
//                    public void onResponse(Call<GetProfileInfoResponse> call, Response<GetProfileInfoResponse> response) {
//                        Log.d("MainActivity", "onResponse" + response.toString());
//                        Log.d("MainActivity", "onResponse" + response.body());
//
//                    }
//
//                    @Override
//                    public void onFailure(Call<GetProfileInfoResponse> call, Throwable t) {
//                        t.printStackTrace();
//                        Log.d("MainActivity", "onFailure");
//                    }
//                });


            }
        });
        getCurrentDateUTC();
        localToUtcTime();
    }

    public static Date getCurrentDateUTC() {
        try {
            TimeZone timeZoneUTC = TimeZone.getTimeZone("UTC");
            Date localTime = new Date();
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z");
            dateFormat.setTimeZone(timeZoneUTC);
            String dateUTCAsString = dateFormat.format(localTime);
            Date dateResult = dateFormat.parse(dateUTCAsString);
            Log.v("utctime-b", "getCurrentDateUTC: LocalDate = " + localTime);
            Log.v("utctime-b", "getCurrentDateUTC: dateUTCAsString = " + dateUTCAsString);
//            DateFormat dateFormatOne = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z");

            Log.v("utctime-b", "getCurrentDateUTC: dateResult = " + dateResult.toString());
            return dateResult;
        } catch (ParseException e) {
            Log.v("utctime-b", "getCurrentDateUTC: ", e);
            return null;
        }
    }

    public String localToUtcTime() {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss Z");
            sdf.setTimeZone(TimeZone.getDefault());
            Date dtstart = new Date();
            sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
            String startdt = sdf.format(dtstart);
            Date dateResult = sdf.parse(startdt);
            ////
            Calendar calendar = Calendar.getInstance();
            TimeZone timeZone = calendar.getTimeZone();
            timeZone.inDaylightTime(dtstart);
            TimeZone timeZone__ = TimeZone.getTimeZone("UTC");
            boolean b = timeZone.inDaylightTime(dtstart);
            boolean bb = timeZone.useDaylightTime();

            sdf.setTimeZone(timeZone__);
            int aa;

            Log.v("utctime-b", b + "");
            Log.v("utctime-bb", bb + "");

            String date_minus = "";

            if (b) {
                Log.v("utctime-new_bb", startdt);
                String[] ss = startdt.split(":");
                aa = Integer.parseInt(ss[0]) - 1;
                date_minus = aa + ":" + ss[1];
            }else if (bb) {
                Log.v("utctime-new_bb", startdt);
                String[] ss = startdt.split(":");
                aa = Integer.parseInt(ss[0]) - 1;
                date_minus = aa + ":" + ss[1];
            }

            Log.v("utctime-new", date_minus);
            Log.v("utctime-new", startdt);
            Log.v("utctime", startdt);

            /*Date myDate = new Date();

            Calendar calendar = Calendar.getInstance();
            calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
            calendar.setTime(myDate);
            Date time = calendar.getTime();
//            SimpleDateFormat outputFmt = new SimpleDateFormat("MMM dd, yyy h:mm a zz");
            SimpleDateFormat outputFmt = new SimpleDateFormat("HH:mm");
            String dateAsString = outputFmt.format(time);
            System.out.println(dateAsString);

            Log.v("utctime-new", dateAsString);
            Log.v("utctime", dateAsString);*/

            if (b) {
                return date_minus;
            }else if (bb) {
                return date_minus;
            } else {
                return startdt;
            }

//            return dateAsString;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


}