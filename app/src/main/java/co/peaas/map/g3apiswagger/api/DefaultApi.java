package co.peaas.map.g3apiswagger.api;

import co.peaas.map.g3apiswagger.model.AddCreditCardRequest;
import co.peaas.map.g3apiswagger.model.AddCreditCardResponse;
import co.peaas.map.g3apiswagger.model.ChangeProfileRequest;
import co.peaas.map.g3apiswagger.model.ChangeProfileResponse;
import co.peaas.map.g3apiswagger.model.ConfirmPaymentRequest;
import co.peaas.map.g3apiswagger.model.ConfirmPaymentResponse;
import co.peaas.map.g3apiswagger.model.EmailVerificationRequest;
import co.peaas.map.g3apiswagger.model.EmailVerificationResponse;
import co.peaas.map.g3apiswagger.model.GetAccessLocationsRequest;
import co.peaas.map.g3apiswagger.model.GetAccessLocationsResponse;
import co.peaas.map.g3apiswagger.model.GetAccountHistoryRequest;
import co.peaas.map.g3apiswagger.model.GetAccountHistoryResponse;
import co.peaas.map.g3apiswagger.model.GetAllBundlesRequest;
import co.peaas.map.g3apiswagger.model.GetAllBundlesRespone;
import co.peaas.map.g3apiswagger.model.GetAllCreditCardsRequest;
import co.peaas.map.g3apiswagger.model.GetAllCreditCardsResponse;
import co.peaas.map.g3apiswagger.model.GetBalanceRequest;
import co.peaas.map.g3apiswagger.model.GetBalanceResponse;
import co.peaas.map.g3apiswagger.model.GetCdrRequest;
import co.peaas.map.g3apiswagger.model.GetCdrResponse;
import co.peaas.map.g3apiswagger.model.GetConfigurationsRequest;
import co.peaas.map.g3apiswagger.model.GetConfigurationsResponse;
import co.peaas.map.g3apiswagger.model.GetCountryBundlesRequest;
import co.peaas.map.g3apiswagger.model.GetCountryBundlesResponse;
import co.peaas.map.g3apiswagger.model.GetLanRequest;
import co.peaas.map.g3apiswagger.model.GetLanResponse;
import co.peaas.map.g3apiswagger.model.GetLocalAccessNumberResponse;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoIAPRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoIAPResponse;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentInfoResponse;
import co.peaas.map.g3apiswagger.model.GetPaymentMethodRequest;
import co.peaas.map.g3apiswagger.model.GetPaymentMethodResponse;
import co.peaas.map.g3apiswagger.model.GetProfileInfoRequest;
import co.peaas.map.g3apiswagger.model.GetProfileInfoResponse;
import co.peaas.map.g3apiswagger.model.GetRateRequest;
import co.peaas.map.g3apiswagger.model.GetRateResponse;
import co.peaas.map.g3apiswagger.model.GetRechargeAmountsRequest;
import co.peaas.map.g3apiswagger.model.GetRechargeAmountsResponse;
import co.peaas.map.g3apiswagger.model.GetSpecialOfferRequest;
import co.peaas.map.g3apiswagger.model.GetSpecialOfferResponse;
import co.peaas.map.g3apiswagger.model.GetWorldBundlesRequest;
import co.peaas.map.g3apiswagger.model.GetWorldBundlesResponse;
import co.peaas.map.g3apiswagger.model.IAPTopUpRequest;
import co.peaas.map.g3apiswagger.model.InitProvisionRequest;
import co.peaas.map.g3apiswagger.model.InitProvisionResponse;
import co.peaas.map.g3apiswagger.model.InitTopUpResponse;
import co.peaas.map.g3apiswagger.model.RedeemVoucherRequest;
import co.peaas.map.g3apiswagger.model.ReferFriendRequest;
import co.peaas.map.g3apiswagger.model.ReferFriendResponse;
import co.peaas.map.g3apiswagger.model.ResendEmailRequest;
import co.peaas.map.g3apiswagger.model.ResendEmailResponse;
import co.peaas.map.g3apiswagger.model.ResendOTPRequest;
import co.peaas.map.g3apiswagger.model.ResendOTPResponse;
import co.peaas.map.g3apiswagger.model.SetUpCallForwardRequest;
import co.peaas.map.g3apiswagger.model.SetUpCallForwardResponse;
import co.peaas.map.g3apiswagger.model.UpdatePushTokenRequest;
import co.peaas.map.g3apiswagger.model.UpdatePushTokenResponse;
import co.peaas.map.g3apiswagger.model.VerifyOtpRequest;
import co.peaas.map.g3apiswagger.model.VerifyOtpResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


public interface DefaultApi {
  /**
   * Add credit card info
   * Returns credit card info.
   * @param addCreditCard Returns credit card info. (optional)
   * @return Call&lt;AddCreditCardResponse&gt;
   */
  
  @POST("AddCreditCard")
  Call<AddCreditCardResponse> addCreditCardPost(
          @Body AddCreditCardRequest addCreditCard
  );

  /**
   * changeProfile
   * Returns user profile related information.
   * @param init A user profile to validate. (optional)
   * @return Call&lt;ChangeProfileResponse&gt;
   */
  
  @POST("changeProfile")
  Call<ChangeProfileResponse> changeProfilePost(
          @Body ChangeProfileRequest init
  );

  /**
   * ConfirmPayment
   * Returns a Confirm Payment related information.
   * @param confirmPaymentRequest A person to validate. (optional)
   * @return Call&lt;ConfirmPaymentResponse&gt;
   */
  
  @POST("ConfirmPayment")
  Call<ConfirmPaymentResponse> confirmPaymentPost(
          @Body ConfirmPaymentRequest confirmPaymentRequest
  );

  /**
   * Email Verification Check
   * Email Verification Check
   * @param emailVerificationCheck Email Verification Check (optional)
   * @return Call&lt;EmailVerificationResponse&gt;
   */
  
  @POST("EmailVerificationCheck")
  Call<EmailVerificationResponse> emailVerificationCheckPost(
          @Body EmailVerificationRequest emailVerificationCheck
  );

  /**
   * GetAccessLocationsByState
   * Returns location related information.
   * @param getAccessLocationsByState Returns location related information. (optional)
   * @return Call&lt;GetAccessLocationsResponse&gt;
   */
  
  @POST("GetAccessLocationsByState")
  Call<GetAccessLocationsResponse> getAccessLocationsByStatePost(
          @Body GetAccessLocationsRequest getAccessLocationsByState
  );

  /**
   * GetAccessLocations
   * Returns location related information.
   * @param getAccessLocations Returns location related information. (optional)
   * @return Call&lt;GetAccessLocationsResponse&gt;
   */
  
  @POST("GetAccessLocations")
  Call<GetAccessLocationsResponse> getAccessLocationsPost(
          @Body GetAccessLocationsRequest getAccessLocations
  );

  /**
   * GetAccountHistoryRequest
   * Returns a Account History related information.
   * @param getAccountHistoryRequest A person to validate. (optional)
   * @return Call&lt;GetAccountHistoryResponse&gt;
   */
  
  @POST("GetAccountHistory")
  Call<GetAccountHistoryResponse> getAccountHistoryPost(
          @Body GetAccountHistoryRequest getAccountHistoryRequest
  );

  /**
   * GetAllBundles
   * Returns bundles related information.
   * @param init A bundle to validate. (optional)
   * @return Call&lt;GetAllBundlesRespone&gt;
   */
  
  @POST("GetAllBundles")
  Call<GetAllBundlesRespone> getAllBundlesPost(
          @Body GetAllBundlesRequest init
  );

  /**
   * to get allcreditcardinfo
   * Returns credit card info
   * @param getAllCreditCards Returns credit card info. (optional)
   * @return Call&lt;GetAllCreditCardsResponse&gt;
   */
  
  @POST("GetAllCreditCards")
  Call<GetAllCreditCardsResponse> getAllCreditCardsPost(
          @Body GetAllCreditCardsRequest getAllCreditCards
  );

  /**
   * to get Rechargeamount
   * for get Recharge amount
   * @param getAutoRechargeAmounts for get Recharge amount. (optional)
   * @return Call&lt;GetRechargeAmountsResponse&gt;
   */
  
  @POST("GetAutoRechargeAmounts")
  Call<GetRechargeAmountsResponse> getAutoRechargeAmountsPost(
          @Body GetRechargeAmountsRequest getAutoRechargeAmounts
  );

  /**
   * GetBalance
   * Returns balance related information.
   * @param getBalance Returns balance related information. (optional)
   * @return Call&lt;GetBalanceResponse&gt;
   */
  
  @POST("GetBalance")
  Call<GetBalanceResponse> getBalancePost(
          @Body GetBalanceRequest getBalance
  );

  /**
   * GetCdr
   * Returns Cdr related information.
   * @param getCdr Returns Cdr related information. (optional)
   * @return Call&lt;GetCdrResponse&gt;
   */
  
  @POST("GetCdr")
  Call<GetCdrResponse> getCdrPost(
          @Body GetCdrRequest getCdr
  );

  /**
   * Get Configurations
   * GetConfigurations
   * @param getConfigurations Get Configurations (optional)
   * @return Call&lt;GetConfigurationsResponse&gt;
   */
  
  @POST("GetConfigurations")
  Call<GetConfigurationsResponse> getConfigurationsPost(
          @Body GetConfigurationsRequest getConfigurations
  );

  /**
   * to get countrybundle
   * Returns countrybundle
   * @param getCountryBundles Returns countrybundle. (optional)
   * @return Call&lt;GetCountryBundlesResponse&gt;
   */
  
  @POST("GetCountryBundles")
  Call<GetCountryBundlesResponse> getCountryBundlesPost(
          @Body GetCountryBundlesRequest getCountryBundles
  );

  /**
   * GetLan
   * Get LAN INfo.
   * @param getLanRequest Get LAN information. (optional)
   * @return Call&lt;GetLanResponse&gt;
   */
  
  @POST("GetLan")
  Call<GetLanResponse> getLanPost(
          @Body GetLanRequest getLanRequest
  );

  /**
   * GetLocalAccessNumber
   * Returns LocalAccessNumber related information.
   * @param getLocalAccessNumber Returns LocalAccessNumber related information. (optional)
   * @return Call&lt;GetLocalAccessNumberResponse&gt;
   */
  
  @POST("GetLocalAccessNumber")
  Call<GetLocalAccessNumberResponse> getLocalAccessNumberPost(
          @Body GetAccessLocationsRequest getLocalAccessNumber
  );

  /**
   * Ge tPayment Info InApp
   * Get Payment Info.
   * @param getPaymentInfoInApp Get Payment Info. (optional)
   * @return Call&lt;GetPaymentInfoIAPResponse&gt;
   */
  
  @POST("GetPaymentInfoInApp")
  Call<GetPaymentInfoIAPResponse> getPaymentInfoInAppPost(
          @Body GetPaymentInfoIAPRequest getPaymentInfoInApp
  );

  /**
   * GetPaymentInfo
   * Returns a Payment Info related information.
   * @param getPaymentInfoRequest A person to validate. (optional)
   * @return Call&lt;GetPaymentInfoResponse&gt;
   */
  
  @POST("GetPaymentInfo")
  Call<GetPaymentInfoResponse> getPaymentInfoPost(
          @Body GetPaymentInfoRequest getPaymentInfoRequest
  );

  /**
   * GetPaymentMethod
   * Returns a Payment Method related information.
   * @param getPaymentMethodRequestRequest A person to validate. (optional)
   * @return Call&lt;GetPaymentMethodResponse&gt;
   */
  
  @POST("GetPaymentMethod")
  Call<GetPaymentMethodResponse> getPaymentMethodPost(
          @Body GetPaymentMethodRequest getPaymentMethodRequestRequest
  );

  /**
   * GetProfileInfoRequest
   * Returns a user Profile related information.
   * @param getProfileInfoRequest A person to validate. (optional)
   * @return Call&lt;GetProfileInfoResponse&gt;
   */
  
  @POST("GetProfileInfo")
  Call<GetProfileInfoResponse> getProfileInfoPost(
          @Body GetProfileInfoRequest getProfileInfoRequest
  );

  /**
   * GetRate
   * Returns Rate related information.
   * @param getRate Returns Rate related information. (optional)
   * @return Call&lt;GetRateResponse&gt;
   */
  
  @POST("GetRate")
  Call<GetRateResponse> getRatePost(
          @Body GetRateRequest getRate
  );

  /**
   * to get Rechargeamount
   * for get Recharge amount
   * @param getRechargeAmounts for get Recharge amount. (optional)
   * @return Call&lt;GetRechargeAmountsResponse&gt;
   */
  
  @POST("GetRechargeAmounts")
  Call<GetRechargeAmountsResponse> getRechargeAmountsPost(
          @Body GetRechargeAmountsRequest getRechargeAmounts
  );

  /**
   * GetSpecialOfferRequest
   * Returns a Special Offer related information.
   * @param getSpecialOfferRequest A person to validate. (optional)
   * @return Call&lt;GetSpecialOfferResponse&gt;
   */
  
  @POST("GetSpecialOffer")
  Call<GetSpecialOfferResponse> getSpecialOfferPost(
          @Body GetSpecialOfferRequest getSpecialOfferRequest
  );

  /**
   * Get World Bundles
   * Get World Bundles
   * @param getWorldBundles Get World Bundles (optional)
   * @return Call&lt;GetWorldBundlesResponse&gt;
   */
  
  @POST("GetWorldBundles")
  Call<GetWorldBundlesResponse> getWorldBundlesPost(
          @Body GetWorldBundlesRequest getWorldBundles
  );

  /**
   * IAP Subscribe
   * IAP Subscribe
   * @param iaPSubscribe IAP Subscribe (optional)
   * @return Call&lt;ConfirmPaymentResponse&gt;
   */
  
  @POST("IAPSubscribe")
  Call<ConfirmPaymentResponse> iAPSubscribePost(
          @Body IAPTopUpRequest iaPSubscribe
  );

  /**
   * InApp Topup
   * InApp Topup
   * @param iaPTopUp InApp Topup (optional)
   * @return Call&lt;ConfirmPaymentResponse&gt;
   */
  
  @POST("IAPTopUp")
  Call<ConfirmPaymentResponse> iAPTopUpPost(
          @Body IAPTopUpRequest iaPTopUp
  );

  /**
   * InitProvisioning
   * Returns a user related information.
   * @param init A person to validate. (optional)
   * @return Call&lt;InitProvisionResponse&gt;
   */
  
  @POST("InitProvisioning")
  Call<InitProvisionResponse> initProvisioningPost(
          @Body InitProvisionRequest init
  );

  /**
   * InitTopUp
   * Returns a Init Top Up related information.
   * @param iaPTopUpRequest A person to validate. (optional)
   * @return Call&lt;InitTopUpResponse&gt;
   */
  
  @POST("InitTopUp")
  Call<InitTopUpResponse> initTopUpPost(
          @Body IAPTopUpRequest iaPTopUpRequest
  );

  /**
   * RedeemVoucher
   * Returns a Redeem Voucher related information.
   * @param redeemVoucherRequest A person to validate. (optional)
   * @return Call&lt;ConfirmPaymentResponse&gt;
   */
  
  @POST("RedeemVoucher")
  Call<ConfirmPaymentResponse> redeemVoucherPost(
          @Body RedeemVoucherRequest redeemVoucherRequest
  );

  /**
   * ReferFriend
   * Returns confirmation message about refer a friend.
   * @param init A person to validate. (optional)
   * @return Call&lt;ReferFriendResponse&gt;
   */
  
  @POST("ReferFriend")
  Call<ReferFriendResponse> referFriendPost(
          @Body ReferFriendRequest init
  );

  /**
   * Resend Email
   * ResendEmail
   * @param resendEmail Resend Email (optional)
   * @return Call&lt;ResendEmailResponse&gt;
   */
  
  @POST("ResendEmail")
  Call<ResendEmailResponse> resendEmailPost(
          @Body ResendEmailRequest resendEmail
  );

  /**
   * ResendOtp
   * Returns a OTP related information.
   * @param init A person to validate. (optional)
   * @return Call&lt;ResendOTPResponse&gt;
   */
  
  @POST("ResendOtp")
  Call<ResendOTPResponse> resendOtpPost(
          @Body ResendOTPRequest init
  );

  /**
   * to setupcallforwarding
   * for set call forwarding
   * @param setUpCallForward Returns credit card info. (optional)
   * @return Call&lt;SetUpCallForwardResponse&gt;
   */
  
  @POST("SetUpCallForward")
  Call<SetUpCallForwardResponse> setUpCallForwardPost(
          @Body SetUpCallForwardRequest setUpCallForward
  );

  /**
   * UpdatePushToken
   * Returns push token related information.
   * @param updatePushToken Returns push token related information. (optional)
   * @return Call&lt;UpdatePushTokenResponse&gt;
   */
  
  @POST("UpdatePushToken")
  Call<UpdatePushTokenResponse> updatePushTokenPost(
          @Body UpdatePushTokenRequest updatePushToken
  );

  /**
   * verifyOtp
   * Returns a OTP related information.
   * @param init A person to validate. (optional)
   * @return Call&lt;VerifyOtpResponse&gt;
   */
  
  @POST("verifyOtp")
  Call<VerifyOtpResponse> verifyOtpPost(
          @Body VerifyOtpRequest init
  );

}
