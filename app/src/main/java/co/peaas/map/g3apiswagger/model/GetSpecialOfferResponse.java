/*
 * G3 API
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package co.peaas.map.g3apiswagger.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

/**
 * GetSpecialOfferResponse
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2017-11-09T12:19:59.555+05:30")
public class GetSpecialOfferResponse extends CommonAPIResponse {
  @SerializedName("imageUrl")
  private String imageUrl = null;

  @SerializedName("launchUrl")
  private String launchUrl = null;

  @SerializedName("ani")
  private String ani = null;

  public GetSpecialOfferResponse imageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
    return this;
  }

   /**
   * Get imageUrl
   * @return imageUrl
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getImageUrl() {
    return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
    this.imageUrl = imageUrl;
  }

  public GetSpecialOfferResponse launchUrl(String launchUrl) {
    this.launchUrl = launchUrl;
    return this;
  }

   /**
   * Get launchUrl
   * @return launchUrl
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getLaunchUrl() {
    return launchUrl;
  }

  public void setLaunchUrl(String launchUrl) {
    this.launchUrl = launchUrl;
  }

  public GetSpecialOfferResponse ani(String ani) {
    this.ani = ani;
    return this;
  }

   /**
   * Get ani
   * @return ani
  **/
  @ApiModelProperty(example = "null", value = "")
  public String getAni() {
    return ani;
  }

  public void setAni(String ani) {
    this.ani = ani;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GetSpecialOfferResponse getSpecialOfferResponse = (GetSpecialOfferResponse) o;
    return Objects.equals(this.imageUrl, getSpecialOfferResponse.imageUrl) &&
        Objects.equals(this.launchUrl, getSpecialOfferResponse.launchUrl) &&
        Objects.equals(this.ani, getSpecialOfferResponse.ani) &&
        super.equals(o);
  }

  @Override
  public int hashCode() {
    return Objects.hash(imageUrl, launchUrl, ani, super.hashCode());
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class GetSpecialOfferResponse {\n");
    sb.append("    ").append(toIndentedString(super.toString())).append("\n");
    sb.append("    imageUrl: ").append(toIndentedString(imageUrl)).append("\n");
    sb.append("    launchUrl: ").append(toIndentedString(launchUrl)).append("\n");
    sb.append("    ani: ").append(toIndentedString(ani)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

